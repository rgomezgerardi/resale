# Resale
A first-hand pixel art typeface

<picture >
  <source media="(prefers-color-scheme: dark)" srcset="resale-dark.png">
  <source media="(prefers-color-scheme: light)" srcset="resale-light.png">
  <img align="right" alt="Specifying the theme an image is shown to" src="resale-light.png">
</picture>
